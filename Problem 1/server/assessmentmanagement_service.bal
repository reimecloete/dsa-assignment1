import ballerina/grpc;
import ballerina/log;

//listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR_MANAGEMENT, descMap: getDescriptorMapManagement()}
service "AssessmentManagement" on new grpc:Listener(9090) {
    //simple
    remote function assign_courses(Courses value) returns string|error {
        return value.CourseName;
    }
    remote function submit_marks(Assessor1 value) returns int|error {
        return value.SubmitMarks;
    }

    //client
    remote function create_users(stream<UserInfo2, grpc:Error?> clientStream) returns string|error {
      check clientStream.forEach(function(UserInfo2 profile){
       
      });
      return "User successfully created";
    }
    remote function submit_assignments(stream<Assignments, grpc:Error?> clientStream) returns string|error {
        check clientStream.forEach(function(Assignments p){
       
      });
      return "User successfully created";
    }
    remote function register(stream<Courses, grpc:Error?> clientStream) returns string|error {
        check clientStream.forEach(function(Courses c){
       
      });
      return "User successfully created";
    }

    
    //bi 
   

    //server
    remote function request_assignments(string value) returns stream<Assignments, error?>|error {
        log:printInfo("Request an assignment: '"+value+"'");
        Assignments[] v1= [];

        return v1.toStream();
        
     }

      remote function create_courses(stream<Courses, grpc:Error?> clientStream) returns stream<string, error?>|error {
      AssessmentManagementStringCaller caller;
      check clientStream.forEach(function(Courses receivedCourse){
        string pkey = (receivedCourse.CourseName);
        lock{
            string? newC;
            if newC is string{
                foreach string sendCourse in newC {
                    grpc:Error? e = caller->sendString(" ");
                }
                 newC.push(receivedCourse);
                receivedCourse.CourseName = newC;

            } else {
                receivedCourse.CourseName = "Course created";
            }
        }
      });
         
    }
}

