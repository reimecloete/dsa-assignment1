import ballerina/grpc;
import ballerina/protobuf.types.wrappers;

public isolated client class AssessmentManagementClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_MANAGEMENT, getDescriptorMapManagement());
    }

    isolated remote function assign_courses(Courses|ContextCourses req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Courses message;
        if req is ContextCourses {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagement/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function assign_coursesContext(Courses|ContextCourses req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Courses message;
        if req is ContextCourses {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagement/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function submit_marks(Assessor1|ContextAssessor1 req) returns int|grpc:Error {
        map<string|string[]> headers = {};
        Assessor1 message;
        if req is ContextAssessor1 {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagement/submit_marks", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <int>result;
    }

    isolated remote function submit_marksContext(Assessor1|ContextAssessor1 req) returns wrappers:ContextInt|grpc:Error {
        map<string|string[]> headers = {};
        Assessor1 message;
        if req is ContextAssessor1 {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("AssessmentManagement/submit_marks", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <int>result, headers: respHeaders};
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagement/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagement/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("AssessmentManagement/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function request_assignments(string|wrappers:ContextString req) returns stream<Assignments, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("AssessmentManagement/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        AssignmentsStream outputStream = new AssignmentsStream(result);
        return new stream<Assignments, grpc:Error?>(outputStream);
    }

    isolated remote function request_assignmentsContext(string|wrappers:ContextString req) returns ContextAssignmentsStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("AssessmentManagement/request_assignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        AssignmentsStream outputStream = new AssignmentsStream(result);
        return {content: new stream<Assignments, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("AssessmentManagement/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUserInfo2(UserInfo2 message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUserInfo2(ContextUserInfo2 message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAssignments(Assignments message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAssignments(ContextAssignments message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourses(Courses message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourses(ContextCourses message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class AssignmentsStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|Assignments value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|Assignments value;|} nextRecord = {value: <Assignments>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourses(Courses message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourses(ContextCourses message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class AssessmentManagementAssignmentsCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAssignments(Assignments response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAssignments(ContextAssignments response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementIntCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendInt(int response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextInt(wrappers:ContextInt response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextAssignmentsStream record {|
    stream<Assignments, error?> content;
    map<string|string[]> headers;
|};

public type ContextCoursesStream record {|
    stream<Courses, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserInfo2Stream record {|
    stream<UserInfo2, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssignments record {|
    Assignments content;
    map<string|string[]> headers;
|};

public type ContextCourses record {|
    Courses content;
    map<string|string[]> headers;
|};

public type ContextAssessor1 record {|
    Assessor1 content;
    map<string|string[]> headers;
|};

public type ContextUserInfo2 record {|
    UserInfo2 content;
    map<string|string[]> headers;
|};

public type User record {|
    User_UserType 'type = Learner;
|};

public enum User_UserType {
    Learner,
    Administrator,
    Assessor
}

public type Assignments record {|
    string assignment = "";
|};

public type Courses record {|
    string CourseName = "";
    int NumberOfAssessments = 0;
    int Assessment_weight = 0;
|};

public type Administrator1 record {|
    Courses[] courses = [];
    string AssignedTo = "";
    string id = "";
|};

public type Assessor1 record {|
    string CourseAssigned = "";
    int SubmitMarks = 0;
    string id = "";
|};

public type Learner1 record {|
    Courses[] register = [];
    string Submit = "";
    Assignments[] assignment = [];
    string id = "";
|};

public type UserInfo2 record {|
    string name = "";
    User user = {};
    string id = "";
|};

const string ROOT_DESCRIPTOR_MANAGEMENT = "0A106D616E6167656D656E742E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22640A045573657212220A047479706518032001280E320E2E557365722E557365725479706552047479706522380A085573657254797065120B0A074C6561726E6572100012110A0D41646D696E6973747261746F721001120C0A084173736573736F721002224A0A0955736572496E666F3212120A046E616D6518012001280952046E616D6512190A047573657218022001280B32052E55736572520475736572120E0A026964180320012809520269642288010A07436F7572736573121E0A0A436F757273654E616D65180120012809520A436F757273654E616D6512300A134E756D6265724F664173736573736D656E747318022001280552134E756D6265724F664173736573736D656E7473122B0A114173736573736D656E745F77656967687418032001280552104173736573736D656E7457656967687422640A0E41646D696E6973747261746F723112220A07636F757273657318012003280B32082E436F75727365735207636F7572736573121E0A0A41737369676E6564546F180220012809520A41737369676E6564546F120E0A0269641803200128095202696422650A094173736573736F723112260A0E436F7572736541737369676E6564180120012809520E436F7572736541737369676E656412200A0B5375626D69744D61726B73180220012805520B5375626D69744D61726B73120E0A026964180320012809520269642286010A084C6561726E65723112240A08726567697374657218012003280B32082E436F75727365735208726567697374657212160A065375626D697418022001280952065375626D6974122C0A0A61737369676E6D656E7418032003280B320C2E41737369676E6D656E7473520A61737369676E6D656E74120E0A02696418042001280952026964222D0A0B41737369676E6D656E7473121E0A0A61737369676E6D656E74180120012809520A61737369676E6D656E7432C2030A144173736573736D656E744D616E6167656D656E7412380A0E61737369676E5F636F757273657312082E436F75727365731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512370A0C7375626D69745F6D61726B73120A2E4173736573736F72311A1B2E676F6F676C652E70726F746F6275662E496E74333256616C7565123A0A0C6372656174655F7573657273120A2E55736572496E666F321A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112420A127375626D69745F61737369676E6D656E7473120C2E41737369676E6D656E74731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112340A08726567697374657212082E436F75727365731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112430A13726571756573745F61737369676E6D656E7473121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0C2E41737369676E6D656E74733001123C0A0E6372656174655F636F757273657312082E436F75727365731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756528013001620670726F746F33";

public isolated function getDescriptorMapManagement() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "management.proto": "0A106D616E6167656D656E742E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22640A045573657212220A047479706518032001280E320E2E557365722E557365725479706552047479706522380A085573657254797065120B0A074C6561726E6572100012110A0D41646D696E6973747261746F721001120C0A084173736573736F721002224A0A0955736572496E666F3212120A046E616D6518012001280952046E616D6512190A047573657218022001280B32052E55736572520475736572120E0A026964180320012809520269642288010A07436F7572736573121E0A0A436F757273654E616D65180120012809520A436F757273654E616D6512300A134E756D6265724F664173736573736D656E747318022001280552134E756D6265724F664173736573736D656E7473122B0A114173736573736D656E745F77656967687418032001280552104173736573736D656E7457656967687422640A0E41646D696E6973747261746F723112220A07636F757273657318012003280B32082E436F75727365735207636F7572736573121E0A0A41737369676E6564546F180220012809520A41737369676E6564546F120E0A0269641803200128095202696422650A094173736573736F723112260A0E436F7572736541737369676E6564180120012809520E436F7572736541737369676E656412200A0B5375626D69744D61726B73180220012805520B5375626D69744D61726B73120E0A026964180320012809520269642286010A084C6561726E65723112240A08726567697374657218012003280B32082E436F75727365735208726567697374657212160A065375626D697418022001280952065375626D6974122C0A0A61737369676E6D656E7418032003280B320C2E41737369676E6D656E7473520A61737369676E6D656E74120E0A02696418042001280952026964222D0A0B41737369676E6D656E7473121E0A0A61737369676E6D656E74180120012809520A61737369676E6D656E7432C2030A144173736573736D656E744D616E6167656D656E7412380A0E61737369676E5F636F757273657312082E436F75727365731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512370A0C7375626D69745F6D61726B73120A2E4173736573736F72311A1B2E676F6F676C652E70726F746F6275662E496E74333256616C7565123A0A0C6372656174655F7573657273120A2E55736572496E666F321A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112420A127375626D69745F61737369676E6D656E7473120C2E41737369676E6D656E74731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112340A08726567697374657212082E436F75727365731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112430A13726571756573745F61737369676E6D656E7473121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0C2E41737369676E6D656E74733001123C0A0E6372656174655F636F757273657312082E436F75727365731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756528013001620670726F746F33"};
}

