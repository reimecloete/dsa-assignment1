import ballerina/io;
import ballerina/grpc;
AssessmentManagementClient ep = check new ("http://localhost:9090");

public function main() returns error? {

    Courses course = {CourseName:"Biology",NumberOfAssessments: 4, Assessment_weight: 78};
    //Administrator1 assign ={courses: [], AssignedTo: "Dave"};
    Assessor1 marks1 ={CourseAssigned:"biology",SubmitMarks: 50,id: "145"};
    //Assignments p = {assignment: "MathAssignment"};

    io:println("----Simple RPC----");
    string assessor = check ep->assign_courses(course);
    io:println("Course assigned: ",marks1);
    
    
    int marks = check ep->submit_marks(marks1);
    io:println("Marks submitted: ",marks1);



    io:println("----Client Side Streaming----");
    // Create_usersStreamingClient create_usersStreamingClient = check ep->create_users();
    Create_usersStreamingClient streamingClient = check ep ->create_users();
    UserInfo2[] userValues = [
       {
        name: "Reime",
        user: {'type: Learner}
       },
       {
        name: "John",
        user: {'type: Assessor}
       },
       {
        name: "Jane",
        user: {'type: Administrator}
       }
    ];

    foreach var val in userValues {
        grpc:Error? connErr = streamingClient->sendUserInfo2(val);
        
    }

    grpc:Error? result =streamingClient->complete();
    string|grpc:Error? reponse = streamingClient->receiveString();
    if !(result is ()){
        io:println("Created user: ", result);
    }

    Submit_assignmentsStreamingClient subStreamingClient = check ep->submit_assignments();
    Assignments[] assignment1 = [
        {assignment: "Biology"},
        {assignment: "Physics"}

    ];

    foreach var val in assignment1 {
         grpc:Error? connErr = subStreamingClient->sendAssignments(val);
    }

    grpc:Error? result1 =subStreamingClient->complete();
    string|grpc:Error? reponse1 = subStreamingClient->receiveString();
    if !(result is ()){
        io:println("Added Assignment: ", result1);
    }

    RegisterStreamingClient regStreamingClient = check ep->register();
    Courses[] reg =[
        {CourseName: "Math",
        NumberOfAssessments: 5,
        Assessment_weight: 5},
        {CourseName: "English",
        NumberOfAssessments: 5,
        Assessment_weight: 5}

    ];
    foreach var val in reg {
         grpc:Error? connErr = regStreamingClient->sendCourses(val);
    }

    grpc:Error? result2 =regStreamingClient->complete();
    string|grpc:Error? reponse2 = regStreamingClient->receiveString();
    if !(result is ()){
        io:println("Added Assignment: ", result2);
    }

     io:println("----Server Side Streaming----");
    stream<Assignments,grpc:Error?> streamAssignments = check ep->request_assignments("doc");
    check streamAssignments.forEach(function(Assignments doc){
    
         io:println("Assignment: ", doc);
    });

     io:println("----Bi-Directional Streaming----");

     Courses[] newCourses=[
        {CourseName: "Physics",
        NumberOfAssessments: 5,
        Assessment_weight: 5},
        {CourseName: "Geography",
        NumberOfAssessments: 5,
        Assessment_weight: 5}
      
     ];

     Create_coursesStreamingClient createStreamingClient = check ep->create_courses();

     future<error?> f1 = start readResponse(createStreamingClient);

     foreach Courses n in newCourses {
        check createStreamingClient->sendCourses(n);
        
     }

     check createStreamingClient->complete();

     check wait f1;
}
     //readresponse function
     function readResponse(Create_coursesStreamingClient createClient) returns error?{
        string? receiveString = check createClient->receiveString();
        while receiveString !=(){
           // io:println('course =${receiveString}');
            receiveString = check createClient->receiveString();
        }
     }
